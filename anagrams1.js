/*
Main goal: be able to take any word and return an anagram of it
List of words contained in words.js file, words[] array.
When button is clicked:  1)get word entered, 2) split word into letters
    3) alphabetize letters 4) find all words in words[] array with same letters
    5) return the words in their own array 6) print the words on screen
*/

const button = document.getElementById("findButton");
button.onclick = function () {
    let typedText = document.getElementById("input").value;

    function alphabetize(a) {
        return a.toLowerCase().split("").sort().join("").trim();
    }
    
    const anagramsOfInputWord = []
    
    function getAnagramsOf(text) {
        for (let i = 0; i < words.length; i++) {
            alphabetize(text)
            currentWord = words[i];
            let wordInArray = alphabetize(currentWord);
            if (wordInArray === alphabetize(text)) {
                anagramsOfInputWord.push(currentWord)
            }
        }
        return anagramsOfInputWord
    }

    let span = document.createElement('span');
    let wordsToPrint = document.createTextNode(getAnagramsOf(typedText));
    span.appendChild(wordsToPrint);
    document.body.appendChild(span);
}









